---
- hosts: service
  vars_files:
    - ldap-vault.yml
  vars:
    timezone: Europe/London
    locale: en_GB.UTF-8
    slapd_configured_file: /etc/ldap/slapd.deb.configured.ansible
    realm_created_file: /var/lib/ldap/realm_created.ansible
    ldap_admin_password: G9$N89NZz46w995z
    ldap_principle: 'service-01.1pcdev.com'
  roles:
    - setup
    - security
  handlers:
    - name: reload system ipset
      shell: cat /etc/ipset/system | ipset -! restore
    - name: restart slapd
      service:
        name: slapd
        state: restarted

  tasks:
    - name: install ldap packages
      apt:
        name: '{{ item }}'
      with_items:
        - slapd
        - ldap-utils
        - openssl
        - krb5-kdc-ldap
        - gnutls-bin
        - ssl-cert
        - krb5-admin-server
        - krb5-kdc
      tags: kerberos

    - name: add all hosts in group to /etc/hosts
      lineinfile:
        dest: /etc/hosts
        line: "{{ hostvars[item]['ansible_eth0']['ipv4']['address']}} {{ item }}"
      with_items: "{{groups['service']}}"

    - name: check if slapd has been reconfigured
      stat:
        path: '{{ slapd_configured_file }}'
      register: slapd_already_configured

    - debug: var=slapd_already_configured

    - name: preseed the slapd deb config
      shell: "echo '{{item.question}} {{item.answer}}' | debconf-set-selections"
      with_items:
        - {question: 'slapd slapd/password1 password', answer: '{{ ldap_admin_password }}' }
        - {question: 'slapd slapd/password2 password', answer: '{{ ldap_admin_password }}' }
        - {question: 'slapd shared/organization string', answer: '1partcarbon.com'}
        - {question: 'slapd slapd/backend select', answer: 'MDB'}
        - {question: 'slapd slapd/move_old_database boolean', answer: 'true'}
        - {question: 'slapd slapd/purge_database  boolean ', answer: 'true'}
        - {question: 'slapd slapd/domain  string', answer: '1partcarbon.com'}
        - {question: 'slapd slapd/allow_ldap_v2 boolean', answer: 'false'}
      when: not slapd_already_configured.stat.exists

    - name: reconfigure slapd
      command: dpkg-reconfigure --frontend noninteractive slapd
      register: slapd_reconfigured_success
      when: not slapd_already_configured.stat.exists

    - debug: var=slapd_reconfigured_success
      when: slapd_reconfigured_success is defined

    - name: touch '{{slapd_configured_file}}'
      file:
        path: '{{ slapd_configured_file }}'
        state: touch
      when: slapd_reconfigured_success.changed

    - name: open firewall port 389
      lineinfile:
        line: 'add system 389'
        dest: /etc/ipset/system
      register: port_389_open
      notify: reload system ipset

    - name: generate ca key
      shell: certtool --generate-privkey > /etc/ssl/private/cakey.pem

    - name: copy cert signing info over
      template:
        src: ca.info.j2
        dest: /etc/ssl/ca.info

    - name: create self signed ca cert
      command: certtool --generate-self-signed --load-privkey /etc/ssl/private/cakey.pem --template /etc/ssl/ca.info --outfile /etc/ssl/certs/cacert.pem

    - name: create a private key for the server
      command: certtool --generate-privkey --bits 1024 --outfile /etc/ssl/private/{{ansible_hostname}}_slapd_key.pem

    - name: copy csr info over
      template:
        src: hostkey_csr.info.j2
        dest: /etc/ssl/ldap.info

    - name: create cert file
      command: certtool --generate-certificate --load-privkey /etc/ssl/private/{{ansible_hostname}}_slapd_key.pem --load-ca-certificate /etc/ssl/certs/cacert.pem --load-ca-privkey /etc/ssl/private/cakey.pem --template /etc/ssl/ldap.info --outfile /etc/ssl/certs/{{ansible_hostname}}_slapd_cert.pem

    - user:
        name: openldap
        groups: ssl-cert

    - name: copy replication config to primary
      tags: ldap-replication
      when: ansible_fqdn == ldap_principle
      copy:
        src: templates/provider_sync.ldif
        dest: /var/lib/ldap/provider_sync.ldif
      register: kerberos_repl_primary_copied

    - name: create accesslog directory
      tags: ldap-replication
      when: ansible_fqdn == ldap_principle
      file:
        path: /var/lib/ldap/accesslog
        owner: openldap
        state: directory

    - name: run replication config on primary
      tags: ldap-replication
      when: kerberos_repl_primary_copied.changed and ansible_fqdn == ldap_principle
      command: ldapmodify -Y EXTERNAL -H ldapi:/// -f /var/lib/ldap/provider_sync.ldif
      notify: restart slapd

    - name: copy replication config to secondary
      tags: ldap-replication
      when: not ansible_fqdn == ldap_principle
      template:
        src: templates/consumer_sync.ldif
        dest: /var/lib/ldap/consumer_sync.ldif
      register: kerberos_repl_secondary_copied

    - name: run replication config on secondary
      tags: ldap-replication
      when: kerberos_repl_secondary_copied.changed and not ansible_fqdn == ldap_principle
      command: ldapadd -c -Y EXTERNAL -H ldapi:/// -f /var/lib/ldap/consumer_sync.ldif
      notify: restart slapd

    - meta: flush_handlers
      tags: ldap-replication

    - file:
        path: /etc/ssl/private/{{ansible_hostname}}_slapd_key.pem
        group: ssl-cert
        mode: u=rw,g=r

    - name: copy over the certinfo.ldif file
      template:
        src: certinfo.ldif.j2
        dest: /var/lib/ldap/certinfo.ldif
        force: no
      register: installed_certinfo


    - name: execute certinfo ldif
      command: ldapmodify -Y EXTERNAL -H ldapi:/// -f /var/lib/ldap/certinfo.ldif
      when: installed_certinfo.changed
      notify: restart slapd

    - name: copy kerberos schema ldif
      copy:
        src: templates/kerberos.ldif
        dest: /var/lib/ldap/kerberos.ldif
      tags: kerberos
      register: kerberos_schema_copied

    - name: load kerberos schema into ldap
      when: kerberos_schema_copied.changed
      command: ldapadd -Y EXTERNAL -H ldapi:/// -f /var/lib/ldap/kerberos.ldif
      tags: kerberos

    - name: copy krbadmin user ldif
      copy:
        src: templates/create_krb_ldap_admin.ldif
        dest: /var/lib/ldap/create_krb_ldap_admin.ldif
      register: krb_ldap_admin_copied
      tags: kerberos

    - name: add krbadmin ldap user
      when: krb_ldap_admin_copied.changed
      command: 'ldapadd -D cn=admin,dc=1partcarbon,dc=com -w {{ ldap_admin_password }} -H ldapi:/// -f /var/lib/ldap/create_krb_ldap_admin.ldif'
      tags: kerberos

    - name: copy kerberos acls ldif
      copy:
        src: templates/kerberos_acls.ldif
        dest: /var/lib/ldap/kerberos_acls.ldif
      tags: kerberos
      register: kerberos_acls_copied

    - name: load kerberos acls into ldap
      when: kerberos_acls_copied.changed
      command: ldapmodify -Y EXTERNAL -H ldapi:/// -f /var/lib/ldap/kerberos_acls.ldif
      tags: kerberos

    - name: copy krb5 config
      copy:
        src: templates/krb5.conf
        dest: /etc/krb5.conf
        force: yes
      tags: kerberos

    - name: install stash password for krbadmin
      lineinfile:
        line: '{{krbadmin_stash_password}}'
        dest: /etc/krb5kdc/service.keyfile
        create: yes
      tags: kerberos

    - name: check if realm already created
      stat:
        path: '{{ realm_created_file }}'
      register: realm_already_created
      tags: kerberos

    - debug: var=realm_already_created
      tags: kerberos

    - name: create kerberos realm in ldap
      command: 'kdb5_ldap_util -w {{ldap_admin_password}} -D cn=admin,dc=1partcarbon,dc=com create -subtrees dc=1partcarbon,dc=com -r 1PARTCARBON.COM -s -H ldap://service-01.1pcdev.com -P {{kerberos_master_password}}'
      tags: kerberos
      when: not realm_already_created.stat.exists and ansible_fqdn == ldap_principle
      register: realm_created_success

    - name: touch '{{realm_created_file}}'
      file:
        path: '{{ realm_created_file }}'
        state: touch
      when: realm_created_success.changed
      tags: kerberos




